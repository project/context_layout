CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers


INTRODUCTION
------------

The Context Layout module integrates layout functionality with the context
module. It uses the Layout API introduced into core in version 8.3. As a result,
this module will not be compatible with Drupal 8.2 or below.

 * For a full description of the module visit:
   https://www.drupal.org/project/context_layout

 * To submit bug reports and feature suggestions, or to track changes visit:
   https://www.drupal.org/project/issues/context_layout

 * How to register layouts in your theme or project:
   https://www.drupal.org/docs/8/api/layout-api/how-to-register-layouts


REQUIREMENTS
------------

This module requires the following:

 * Context - http://drupal.org/project/context 8.x-4.0-beta2 or higher


INSTALLATION
------------

 * Install the Context Layout module as you would normally install a contributed
   Drupal module. Visit https://www.drupal.org/node/1897420 for further
   information.


CONFIGURATION
-------------

  1. Navigate to Administration > Extend and enable the module.
  2. Register layouts in your theme or module. See
     https://www.drupal.org/docs/8/api/layout-api/how-to-register-layouts
     for more information on how to register layouts.
  3. Add a context block reaction and specify a layout in the "layout"
     dropdown.
  4. Optional: Define global context layout behavior
     (`/admin/config/system/context_layout`)


MAINTAINERS
-----------

 * ellioseven - https://www.drupal.org/u/ellioseven
